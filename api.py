import numpy

class State:
    """
    
    """
    # Data structures
    Objects = {}
    tipcycle = {} # keys: tip types, each key has a list value, list is the order in which boxes are cycled
    ditiloc = {} # keys: tip types, each key has a value indicating the current box in the corresponding list
    
    # Attributes
    verbose = 'mid'
    workspace = None
    mode = 'offline'
    output = None
    tips = 'all'
    tipsloaded = False
    
    # Functions
    def __init__(self):
        self.incubator = Incubator('Incubator', self)
        self.platereader = Platereader('Sunrise', self)
        self.Objects['Incubator'] = self.incubator
        self.Objects['Platereader'] = self.platereader
        #self.output = open('default.esc', 'w')
        return
    
    
    def listObjects(self, category):
        """
        Prints a list of objects present on the robot and their positions
        """
        for key in self.Objects.keys():
            if (self.Objects[key].category == category) | (category == 'all'):
                print str(self.Objects[key].name)+":\t"+str(self.Objects[key].types)+' at '+str(self.Objects[key].grid)+', '+str(self.Objects[key].site)
        return
    
    def setMode(self, mode='offline', pointer=None):
        """
        Parameters:
        --------------------
        string mode
        
        Optional:
        --------------------
        pointer filename or COM port

        Returns:
        --------------------
        None
        
        When offline, commands are exported to a .esc file in the same directory as the script
        When set to online, tries to connect to the robot over the COM port and then issues following commands directly to the robot
        """
        self.mode = mode
        if self.mode == 'offline':
            if pointer == None:
                self.output = open('default.esc', 'w')
            else:
                self.output = open(pointer, 'w')
            f = open(self.workspace)
            lines = f.readlines()
            f.flush()
            f.close()
            for line in lines:
                self.output.writelines(line)
                if line[0:11] == '--{ RPG }--':
                    break
            self.output.flush()
            return
    
    def status(self, category):
        if category == 'tipboxes':
            for key in self.tipcycle.keys():
                order = ''
                for i in range(len(self.tipcycle[key])):
                    order = order + ' ' + self.tipcycle[key][i].name
                print key+': Using box '+self.tipcycle[key][self.ditiloc[key]].name+', from position '+str(self.tipcycle[key][self.ditiloc[key]].nTipsUsed+1)
                print 'Order of boxes is' + order
                return
    
    def setDitiBoxOrder(self, types, order):
        if types in self.tipcycle.keys():
            uidPlate = False
            for labware in order:
                if self.Objects[labware.name].types != types:
                    uidPlate = True
            if uidPlate == False:
                self.tipcycle[types] = order
            else:
                print 'Unidentified tipbox in proposed order!'
        return
    
    def getDiti(self, tips=None, position='auto', types='default', grid=None, site=None, offset=None):
        """
        
        """
        # select a tip type, default is the first type found tipcycle
        tiptype = self.tipcycle.keys()[0] # TODO: add exception if there is no tip box
        if not types == 'default':
            tiptype = types.types #boxtipdict[types]
        loc = self.ditiloc[tiptype]
        #print self.tipcycle[tiptype][loc].name
        # select mask
        if tips == None:
            tips = self.tips
        else:
            self.tips = tips
        if tips == 'all':
            tips = numpy.ones(8)
        if isinstance(tips,list):
            newtips = numpy.zeros(8)
            for i in tips:
                newtips[i-1] = 1
            tips = newtips
        # select position
        if position == 'auto':
            grid, site, offset = self.tipcycle[tiptype][loc].grid, self.tipcycle[tiptype][loc].site, self.tipcycle[tiptype][loc].nTipsUsed+1
            position = (grid, site, offset)
            #print position
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[-i-1] = tips[-i-1]*2**i
        if self.mode == 'offline':
            wstr = "Set_DITI_Counter2(\""+tiptype+"\",\""+str(position[0])+"\",\""+str(position[1])+"\",\""+str(position[2])+"\",0);\n"
            wstr = wstr+"GetDITI2("+str(int(sum(tipval)))+",\""+tiptype+"\",0,0,10,70);\n"
            self.output.write(wstr)
            self.output.flush()
        # update status
        #print tips#, max(numpy.where(tips==1)[0])
        self.tipcycle[tiptype][loc].nTipsUsed = self.tipcycle[tiptype][loc].nTipsUsed + max(numpy.where(tips==1)[0]) + 1 #sum(tips) # skip non-sequential tips
        if self.tipcycle[tiptype][loc].nTipsUsed >= 96:
            self.tipcycle[tiptype][loc].nTipsUsed = 0
            self.ditiloc[tiptype] = (self.ditiloc[tiptype] + 1)%len(self.tipcycle[tiptype])
        self.tipsloaded = True
        return
    
    def setDiti(self, tips=None, position='auto', types='default', grid=None, site=None, offset=None):
        """
        
        """
        # select a tip type, default is the first type found tipcycle
        tiptype = self.tipcycle.keys()[0] # TODO: add exception if there is no tip box
        if not types == 'default':
            tiptype = types.types #boxtipdict[types]
        loc = self.ditiloc[tiptype]
        # select mask
        if tips == None:
            tips = self.tips
        else:
            self.tips = tips
        if tips == 'all':
            tips = numpy.ones(8)
        if isinstance(tips,list):
            newtips = numpy.zeros(8)
            for i in tips:
                newtips[i-1] = 1
            tips = newtips
        # select position
        nReplaced = max(numpy.where(tips==1)[0]) - min(numpy.where(tips==1)[0]) + 1
        #print 'Replace ' + str(nReplaced) + ' tips'
        if position == 'auto':
            if self.tipcycle[tiptype][loc].nTipsUsed >= nReplaced: # loaded tips can fit in current box
                self.tipcycle[tiptype][loc].nTipsUsed = self.tipcycle[tiptype][loc].nTipsUsed - nReplaced
            else: # go to previous box
                self.ditiloc[tiptype] = (self.ditiloc[tiptype] - 1)%len(self.tipcycle[tiptype])
                loc = self.ditiloc[tiptype]
                self.tipcycle[tiptype][loc].nTipsUsed = 96 - nReplaced
            grid, site, offset = self.tipcycle[tiptype][loc].grid, self.tipcycle[tiptype][loc].site, self.tipcycle[tiptype][loc].nTipsUsed+1
            position = (grid, site, offset)
        if isinstance(position,Labware):
            position = (position.grid, position.site, position.nTipsUsed+1)
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[-i-1] = tips[-i-1]*2**i
        if self.mode == 'offline':
            wstr = "Set_DITIs_Back("+str(int(sum(tipval)))+","+str(position[0])+","+str(position[1])+",\""+wellSelGen(position[2],tips,self.tipcycle[tiptype][loc])+"\",0,0);\n"
            self.output.write(wstr)
            self.output.flush()
        self.tipsloaded = False
        return
    
    def mix(self, plate, col, volume, cycles=3, tips=None, liquidClass='Water free dispense', offset=0):
        """
        
        """
        if self.tipsloaded == False:
            print 'No tips loaded'
            return
        # select mask
        if tips == None:
            tips = self.tips
        else:
            self.tips = tips
        if tips == 'all':
            tips = numpy.ones(8)
        if isinstance(tips,list):
            newtips = numpy.zeros(8)
            for i in tips:
                newtips[i-1] = 1
            tips = newtips
        # select volume
        if (isinstance(volume,float)) | (isinstance(volume,int)):
            v = volume
            volume = numpy.zeros(8)
            for i in range(8):
                volume[i] = tips[i]*v
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self.mode == 'offline':
            wstr = "Mix("+str(int(sum(tipval)))+",\""+liquidClass+"\","
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + "0,"
                else:
                    wstr = wstr + "\"" + str(int(volume[i])) +"\","
            wstr = wstr + "0,0,0,0,"
            wstr = wstr+str(plate.grid)+','+str(plate.site)+','+str(1)+','+"\""+wellSelGen((col-1)*8+1+offset,tips,plate)+"\","+str(cycles)+",0,0);\n"
            self.output.write(wstr)
            self.output.flush()
        return
    
    def aspirate(self, plate, col, volume, tips=None, liquidClass='Water free dispense', offset=0):
        """
        
        """
        if self.tipsloaded == False:
            print 'No tips loaded'
            return
        # select mask
        if tips == None:
            tips = self.tips
        else:
            self.tips = tips
        if tips == 'all':
            tips = numpy.ones(8)
        if isinstance(tips,list):
            newtips = numpy.zeros(8)
            for i in tips:
                newtips[i-1] = 1
            tips = newtips
        # select volume
        if (isinstance(volume,float)) | (isinstance(volume,int)):
            v = volume
            volume = numpy.zeros(8)
            for i in range(8):
                volume[i] = tips[i]*v
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self.mode == 'offline':
            wstr = "Aspirate("+str(int(sum(tipval)))+",\""+liquidClass+"\","
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + "0,"
                else:
                    wstr = wstr + "\"" + str(int(volume[i])) +"\","
            wstr = wstr + "0,0,0,0,"
            wstr = wstr+str(plate.grid)+','+str(plate.site)+','+str(1)+','+"\""+wellSelGen((col-1)*8+1+offset,tips,plate)+"\",0,0);\n"
            self.output.write(wstr)
            self.output.flush()
        return
    
    def dispense(self, plate, col, volume, tips=None, liquidClass='Water free dispense', offset=0):
        """
        
        """
        if self.tipsloaded == False:
            print 'No tips loaded'
            return
        # select mask
        if tips == None:
            tips = self.tips
        else:
            self.tips = tips
        if tips == 'all':
            tips = numpy.ones(8)
        if isinstance(tips,list):
            newtips = numpy.zeros(8)
            for i in tips:
                newtips[i-1] = 1
            tips = newtips
        # select volume
        if (isinstance(volume,float)) | (isinstance(volume,int)):
            v = volume
            volume = numpy.zeros(8)
            for i in range(8):
                volume[i] = tips[i]*v
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self.mode == 'offline':
            wstr = "Dispense("+str(int(sum(tipval)))+",\""+liquidClass+"\","
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + "0,"
                else:
                    wstr = wstr + "\"" + str(int(volume[i])) +"\","
            wstr = wstr + "0,0,0,0,"
            wstr = wstr+str(plate.grid)+','+str(plate.site)+','+str(1)+','+"\""+wellSelGen((col-1)*8+1+offset,tips,plate)+"\",0,0);\n"
            self.output.write(wstr)
            self.output.flush()
        return
    
    def dropDiti(self, tips=None):
        """
        
        """
        if self.tipsloaded == False:
            print 'No tips loaded'
            return
        # select mask
        if tips == None:
            tips = self.tips
        else:
            self.tips = tips
        if tips == 'all':
            tips = numpy.ones(8)
        if isinstance(tips,list):
            newtips = numpy.zeros(8)
            for i in tips:
                newtips[i-1] = 1
            tips = newtips
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self.mode == 'offline':
            wstr = "DropDITI("+str(int(sum(tipval)))+','+str(self.Objects['Waste'].grid)+','+str(self.Objects['Waste'].site)+",10,70,0);"
            self.output.write(wstr)
            self.output.flush()
        self.tipsloaded = False
        return
    
    def transferLabware(self, source, destination, cover):
        return
    
    def __del__(self):
        """
        Clean up, close file pointers or log off
        """
        self.output.flush()
        self.output.close()

class Labware:
    """
    
    """
    name = None
    types = None
    grid = None
    site = None
    category = None
    xlen = 12
    ylen = 8
    
    def __init__(self, name, types, grid, site):
        self.name = name
        self.grid = grid
        self.site = site
        self.types = types

class TipBox(Labware):
    """
    
    """
    category = 'tipboxes'
    nTipsUsed = 0

class Trough(Labware):
    """
    
    """
    category = 'troughs'

class Plate(Labware):
    """
    
    """
    category = 'plates'
    occupied = 0

class Device:
    """
    
    """
    name = None
    types = None
    grid = None
    site = None
    parent = None
    category = 'devices'
    
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent

class Incubator(Device):
    """
    
    """
    occupied = numpy.zeros(8)
    
    def closeDoors(self):
        if self.parent.mode == 'offline':
            self.parent.output.write("FACTS(\"Incubator1\",\"Incubator1_CloseDoor\",\"\",\"0\",\"\");\n")
            self.parent.output.flush()
        return
    
    def setTemperature(self, temperature, slots):
        """
        Requires the slots argument to be a list, not an integer
        Say setTemperature(30, [2]), not setTemperature(30, 2)
        """
        if (slots == 'all') | (set(slots) == set([1,2,3,4,5,6])):
            slots = array([0])
        if self.parent.mode == 'offline':
            for slot in slots:
                wstr = "FACTS(\"Incubator1\",\"Incubator1_SetTemperature\",\""
                wstr = wstr+str(slot)+','+str(temperature)+"\",\"0\",\"\");\n"
                self.parent.output.write(wstr)
                self.parent.output.flush()
        #else:
            
        return

class Platereader(Device):
    """
    
    """
    occupied = 0
    
    def close(self):
        if self.parent.mode == 'offline':
            self.parent.output.write("FACTS(\"Sunrise\",\"Sunrise_Close\",\"\",\"0\",\"\");\n")
            self.parent.output.flush()
        return
    
    def open(self):
        if self.parent.mode == 'offline':
            self.parent.output.write("FACTS(\"Sunrise\",\"Sunrise_Open\",\"\",\"0\",\"\");\n")
            self.parent.output.flush()
        return
    
    def measure(self):
        return

def wellSelGen(start, tips, labware):
    """
    start: index where sequence tips starts
    tips: binary sequence
    """
    retstr = ''
    start = start - 1
    xlen = labware.xlen
    ylen = labware.ylen
    retstr = retstr + format(xlen,'02x').upper() + format(ylen,'02x').upper()
    nWells = xlen*ylen
    wells = numpy.zeros(nWells)
    wells[start:start+len(tips)] = tips
    igroup = 0
    while igroup < nWells:
        seq = numpy.zeros(7)
        seq[0:min(7,nWells-igroup)] = wells[igroup:igroup+min(7,nWells-igroup)]
        for i in range(len(seq)):
            seq[i] = seq[i]*2**i
        seqval = int(sum(seq))+48
        retstr = retstr + chr(seqval)
        igroup = igroup + 7
    return retstr


def loadWorkspace(self,file):
    """
    Open the workspace file to read object placements
    and populate the Objects dictionary
    """
    self.workspace = file
    f = open(self.workspace)
    lines = f.readlines()
    f.flush()
    f.close()
    workspaceID = lines[0].strip()
    workspaceModifyDate = lines[1].split()[0].split('_')[0]
    workspaceModifyTime = lines[1].split()[0].split('_')[1]
    WorkspaceCreateName = lines[1].split()[1]
    grid, line = 0, 0
    while grid < 30:
        if lines[line].split(';')[0] == '998':
            types = lines[line].split(';')
            nObj = int(types[1])
            if nObj > 0:
                names = lines[line + 1].split(';')
                site = 0
                for i in range(nObj):
                    if types[2 + i] != '':
                        if types[2 + i].split()[0] == 'DiTi':
                            if not types[2 + i] in self.tipcycle.keys():
                                self.tipcycle[types[2 + i]] = []
                                self.ditiloc[types[2 + i]] = 0
                            self.Objects[names[1 + i]] = TipBox(names[1+i],types[2+i], grid, site)
                            self.tipcycle[types[2 + i]].append(self.Objects[names[1 + i]])
                        elif types[2 + i].split()[0] == 'Trough':
                            self.Objects[names[1 + i]] = Trough(names[1+i],types[2+i], grid, site)
                        elif types[2 + i].split()[2] == 'Microplate':
                            self.Objects[names[1 + i]] = Plate(names[1+i],types[2+i], grid, site)
                        else:
                            self.Objects[names[1 + i]] = Labware(names[1+i],types[2+i], grid, site)
                    site = site + 1
                line = line + 1
            grid = grid + 1
        line = line + 1
    return
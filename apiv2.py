import numpy

class State:
    """
    
    """
    # Data structures
    Objects = {}
    tipcycle = {} # keys: tip types, each key has a list value, list is the order in which boxes are cycled
    ditiloc = {} # keys: tip types, each key has a value indicating the current box in the corresponding list
    
    # Attributes
    verbose = 'mid'
    workspace = None
    mode = 'validate'
    output = None
    tips = numpy.ones(8)
    tipsloaded = False
    loadedtiptype = None
    
    # Functions
    def __init__(self):
        self.incubator = Incubator('Incubator', self)
        self.platereader = Platereader('Sunrise', self)
        self.Objects['Incubator'] = self.incubator
        self.Objects['Platereader'] = self.platereader
        #self.output = open('default.esc', 'w')
        return
    
    def loadWorkspace(self,filename):
        """
        Parameters:
        filename: string, name of .esc file to laod hardware model
        
        Returns:
        None
        
        Open the workspace file to read object placements
        and populate the Objects dictionary
        """
        # read file
        self.workspace = filename
        f = open(self.workspace)
        lines = f.readlines()
        f.flush()
        f.close()
        # read meta. As of now, nothing is done with it
        workspaceID = lines[0].strip()
        workspaceModifyDate = lines[1].split()[0].split('_')[0]
        workspaceModifyTime = lines[1].split()[0].split('_')[1]
        WorkspaceCreateName = lines[1].split()[1]
        # generate workspace model
        # change this if workspace file format changes
        grid, line = 0, 0
        while grid < 30:
            if lines[line].split(';')[0] == '998':
                types = lines[line].split(';')
                nObj = int(types[1])
                if nObj > 0:
                    names = lines[line + 1].split(';')
                    site = 0
                    for i in range(nObj):
                        if types[2 + i] != '':
                            # cycle through all labware types to identify labware
                            # implement dictionary lookup?
                            if types[2+i].lower().startswith('diti') == True:
                                # tip racks: add to appropriate rack cycle
                                if not types[2+i] in self.tipcycle.keys():
                                    self.tipcycle[types[2+i]] = []
                                    self.ditiloc[types[2+i]] = 0
                                self.Objects[names[1+i]] = TipBox(names[1+i],types[2+i], grid, site)
                                self.tipcycle[types[2+i]].append(self.Objects[names[1+i]])
                            elif types[2+i].lower().find('trough') != -1:
                                self.Objects[names[1+i]] = Trough(names[1+i],types[2+i], grid, site)
                            elif types[2+i].lower().find('plate') != -1:
                                self.Objects[names[1+i]] = Plate(names[1+i],types[2+i], grid, site)
                            elif types[2+i].lower().find('tube') != -1:
                                self.Objects[names[1+i]] = Plate(names[1+i],types[2+i], grid, site)
                            elif (types[2+i].lower().startswith('washstation') == True) & (types[2+i].lower().find('diti') != -1):
                                self.Objects['Waste'] = Labware('Waste',types[2+i], grid, site)
                            else:
                                self.Objects[names[1+i]] = Labware(names[1+i],types[2+i], grid, site)
                        site = site + 1
                    line = line + 1
                grid = grid + 1
            line = line + 1
        return
    
    def listObjects(self, category='all'):
        """
        Parameters:
        category: string (='all', 'plates', 'tipboxes', 'troughs', 'devices') default='all'
        
        Returns:
        None
        
        Prints a list of object names, categories, positions present on the robot
        """
        for key in self.Objects.keys():
            if (self.Objects[key].category == category) | (category == 'all'):
                ops = str(self.Objects[key].name)+":\t"+str(self.Objects[key].types)
                ops = ops+' at '+str(self.Objects[key].grid)+', '+str(self.Objects[key].site)
                print ops
        return
    
    def setMode(self, mode='offline', pointer=None):
        """
        Parameters:
        mode: string (='validate', 'offline', 'online')
        pointer: string, filename to write to
              or COM port(not yet defined)

        Returns:
        None
        
        When offline, commands are exported to a .esc file in the same directory as the script
        When set to online, tries to connect to the robot over the COM port and then issues following commands directly to the robot
        """
        # close previous pointers
        if self.mode == 'offline':
            self.output.flush()
            self.output.close()
        #elif self.mode == 'online':
            
        # open new pointers
        self.mode = mode
        if self.mode == 'offline':
            if pointer == None:
                self.output = open('default.esc', 'w')
            else:
                self.output = open(pointer, 'w')
            f = open(self.workspace)
            lines = f.readlines()
            f.flush()
            f.close()
            for line in lines:
                self.output.writelines(line)
                if line[0:11] == '--{ RPG }--':
                    break
            self.output.flush()
            return
        #elif self.mode == 'online':
        return
            
    
    def status(self, category):
        """
        Parameters:
        category: string (='all', 'tipboxes')
        
        Returns:
        None
        
        Prints status information for given type of object
        """
        if (category == 'tipboxes') | (category == 'all'):
            for key in self.tipcycle.keys():
                order = ''
                for i in range(len(self.tipcycle[key])):
                    order = order + ' ' + self.tipcycle[key][i].name
                ops = key+': Using box '+self.tipcycle[key][self.ditiloc[key]].name+', from position '
                ops = ops+str(self.tipcycle[key][self.ditiloc[key]].nTipsUsed+1)
                print ops
                print 'Order of boxes is' + order
        if (category == 'tipboxes') | (category == 'liha'):
            print 'LiHa tip state: ' + numpy.array_str(self.tips)
            if self.tipsloaded == True:
                print 'LiHa tips loaded'
            else:
                print 'LiHa tips not loaded'
            print 'Liha tip type: ' + self.loadedtiptype
        return
    
    def setDitiBoxOrder(self, order, types=None):
        """
        Parameters:
        order: list, list of TipBox objects in cycling order
        types: string, type of tipbox (='DiTi 200ul SBS LiHa', etc)
            or TipBox object, types set to types of TipBox object
            or None (default), uses the type of first tipbox in tipcycle dictionary
        
        Returns:
        None
        
        Sets order of tipboxes of given type to use
        """
        # normalize parameters
        types = self.__normalize__('tips', types)
        
        if types in self.tipcycle.keys():
            uidPlate = False
            for labware in order:
                if self.Objects[labware.name].types != types:
                    uidPlate = True
            if uidPlate == False:
                self.tipcycle[types] = order
            else:
                print 'Unidentified tipbox in proposed order!'
        else:
            print "No tipbox of type '" + types + "' found on robot"
        return
    
    def getDiti(self, tips=None, position=None, col=None, types=None):
        """
        Parameters:
        tips: list, which tips to use (=[1,2,5,6], 1..8)
           or binary array (=array([1,1,0,0,1,1,0,0]))
           or string (='all')
           or None (default), uses previous configuration
        position: TipBox object, uses position of the tipbox
               or None (default), uses next available tips of the type specified
        col: int, column to pick from (1..8)
          or None (default), picks from the first available spot
        types: string, type of tipbox (='DiTi 200ul SBS LiHa', etc)
            or TipBox object, types set to types of TipBox object
            or None (default), uses the type of first tipbox in tipcycle dictionary
        
        Returns:
        None
        
        Picks up (specified) tips (from specified location)
        """
        # normalize
        tiptype = self.__normalize__('tips', types)
        tips = self.__normalize__('mask', tips)
        self.tips = tips
        self.loadedtiptype = tiptype
        loc = self.ditiloc[tiptype]
        
        # update position
        if position == None:
            position = self.tipcycle[tiptype][loc]
        if col != None:
            self.tipcycle[tiptype][loc].nTipsUsed = (col-1)*8
        if 96-self.tipcycle[tiptype][loc].nTipsUsed<max(numpy.where(tips==1)[0])-min(numpy.where(tips==1)[0]): # not enough tips in current box
            loc = (loc + 1)%len(self.tipcycle[tiptype])
            self.ditiloc[tiptype] = loc
            position = self.tipcycle[tiptype][loc]
        coords = (self.tipcycle[tiptype][loc].grid,self.tipcycle[tiptype][loc].site,self.tipcycle[tiptype][loc].nTipsUsed+1)
        
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[-i-1] = tips[-i-1]*2**i
        if self.mode == 'offline':
            wstr = "PickUp_DITIs2("+str(int(sum(tipval)))+","+str(coords[0])+","+str(coords[1])+",\""
            wstr = wstr+wellSelGen(coords[2]-min(numpy.where(tips==1)[0]),tips,self.tipcycle[tiptype][loc])+"\",0,\""+tiptype+"\",0);\n"
            self.output.write(wstr)
            self.output.flush()
        
        # update status
        self.tipcycle[tiptype][loc].nTipsUsed=self.tipcycle[tiptype][loc].nTipsUsed+max(numpy.where(tips==1)[0])-min(numpy.where(tips==1)[0])+1
        if self.tipcycle[tiptype][loc].nTipsUsed >= 96:
            self.tipcycle[tiptype][loc].nTipsUsed = 0
            self.ditiloc[tiptype] = (self.ditiloc[tiptype] + 1)%len(self.tipcycle[tiptype])
        #else
        self.tipsloaded = True
        return
    
    def setDiti(self, position=None):
        """
        Parameters:
        position: TipBox object, uses position of the tipbox
               or None (default), uses next available tips of the type specified
        
        Returns:
        None
        
        Sets back mounted tips (to specified location)
        """
        # normalize
        tiptype = self.loadedtiptype
        tips = self.tips
        loc = self.ditiloc[tiptype]
        self.tips = tips
        
        # select position
        nReplaced = max(numpy.where(tips==1)[0]) - min(numpy.where(tips==1)[0]) + 1
        if position == None:
            if self.tipcycle[tiptype][loc].nTipsUsed >= nReplaced: # loaded tips can fit in current box
                self.tipcycle[tiptype][loc].nTipsUsed = self.tipcycle[tiptype][loc].nTipsUsed - nReplaced
            else: # go to previous box
                self.ditiloc[tiptype] = (self.ditiloc[tiptype] - 1)%len(self.tipcycle[tiptype])
                loc = self.ditiloc[tiptype]
                self.tipcycle[tiptype][loc].nTipsUsed = 96 - nReplaced
            coords = (self.tipcycle[tiptype][loc].grid, self.tipcycle[tiptype][loc].site, self.tipcycle[tiptype][loc].nTipsUsed+1)
        if isinstance(position,Labware):
            coords = (position.grid, position.site, position.nTipsUsed+1)
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[-i-1] = tips[-i-1]*2**i
        if self.mode == 'offline':
            wstr = "Set_DITIs_Back("+str(int(sum(tipval)))+","+str(coords[0])+","+str(coords[1])
            wstr = wstr+",\""+wellSelGen(coords[2],tips,self.tipcycle[tiptype][loc])+"\",0,0);\n"
            self.output.write(wstr)
            self.output.flush()
        #else
        self.tipsloaded = False
        return
    
    def mix(self, plate, col, volume, cycles=3, wells=None, liquidClass='Water free dispense', offset=0):
        """
        Parameters:
        plate: Plate object
            or string, name of labware
        col: int, 1..8
        volume: int, in ul, used for all wells
             or list, in ul, only for wells being used
        cycles: int
        wells: list, which wells to mix (=[1,2,4], 1..8)
            or binary array (=array([1,1,0,1]))
            or None (default), mixes with all loaded tips, can still have an offset
        offset: int, shift the wells pattern up or down by some amount
        liquidClass: string
        
        Returns:
        None
        
        Issue the Mix command
        """
        # normalize
        wells = self.__normalize__('mask', wells)
        volume = self.__normalize__('volume', volume)
        plate = self.__normalize__('plate', plate)
        tips = self.tips
        
        # ensure tips are loaded
        if self.tipsloaded == False:
            print 'No tips loaded'
            return
        
        tips = __genTipOffsetPattern__(tips, wells)
        if isinstance(tips, int):
            print 'Some of the required tips not loaded'
            return
        
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self.mode == 'offline':
            wstr = "Mix("+str(int(sum(tipval)))+",\""+liquidClass+"\","
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + "0,"
                else:
                    wstr = wstr + "\"" + str(int(volume[i])) +"\","
            wstr = wstr + "0,0,0,0,"
            wstr = wstr+str(plate.grid)+','+str(plate.site)+','+str(1)+','+"\""
            wstr = wstr+wellSelGen((col-1)*8+1+offset,tips,plate)+"\","+str(cycles)+",0,0);\n"
            self.output.write(wstr)
            self.output.flush()
        return
    
    def aspirate(self, plate, col, volume, wells=None, liquidClass='Water free dispense', offset=0):
        """
        Parameters:
        plate: Plate object
            or string, name of labware
        col: int, 1..8
        volume: int, in ul, used for all wells
             or list, in ul, only for wells being used
        wells: list, which wells to mix (=[1,2,4], 1..8)
            or binary array (=array([1,1,0,1]))
            or None (default), mixes with all loaded tips, can still have an offset
        offset: int, shift the wells pattern up or down by some amount
        liquidClass: string
        
        Returns:
        None
        
        Issue the Aspirate command
        """
        # normalize
        wells = self.__normalize__('mask', wells)
        volume = self.__normalize__('volume', volume)
        plate = self.__normalize__('plate', plate)
        tips = self.tips
        
        # ensure tips are loaded
        if self.tipsloaded == False:
            print 'No tips loaded'
            return
        
        tips = __genTipOffsetPattern__(tips, wells)
        if isinstance(tips, int):
            print 'Some of the required tips not loaded'
            return
        
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self.mode == 'offline':
            wstr = "Aspirate("+str(int(sum(tipval)))+",\""+liquidClass+"\","
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + "0,"
                else:
                    wstr = wstr + "\"" + str(int(volume[i])) +"\","
            wstr = wstr + "0,0,0,0,"
            wstr = wstr+str(plate.grid)+','+str(plate.site)+','+str(1)+','+"\""+wellSelGen((col-1)*8+1+offset,tips,plate)+"\",0,0);\n"
            self.output.write(wstr)
            self.output.flush()
        return
    
    def dispense(self, plate, col, volume, wells=None, liquidClass='Water free dispense', offset=0):
        """
        Parameters:
        plate: Plate object
            or string, name of labware
        col: int, 1..8
        volume: int, in ul, used for all wells
             or list, in ul, only for wells being used
        wells: list, which wells to mix (=[1,2,4], 1..8)
            or binary array (=array([1,1,0,1]))
            or None (default), mixes with all loaded tips, can still have an offset
        offset: int, shift the wells pattern up or down by some amount
        liquidClass: string
        
        Returns:
        None
        
        Issue the Dispense command
        """
        # normalize
        wells = self.__normalize__('mask', wells)
        volume = self.__normalize__('volume', volume)
        plate = self.__normalize__('plate', plate)
        tips = self.tips
        
        # ensure tips are loaded
        if self.tipsloaded == False:
            print 'No tips loaded'
            return
        
        tips = __genTipOffsetPattern__(tips, wells)
        if isinstance(tips, int):
            print 'Some of the required tips not loaded'
            return
        
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self.mode == 'offline':
            wstr = "Dispense("+str(int(sum(tipval)))+",\""+liquidClass+"\","
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + "0,"
                else:
                    wstr = wstr + "\"" + str(int(volume[i])) +"\","
            wstr = wstr + "0,0,0,0,"
            wstr = wstr+str(plate.grid)+','+str(plate.site)+','+str(1)+','+"\""+wellSelGen((col-1)*8+1+offset,tips,plate)+"\",0,0);\n"
            self.output.write(wstr)
            self.output.flush()
        return
    
    def dropDiti(self, tips=None):
        """
        Parameters:
        tips: list, which tips to use (=[1,2,5,6], 1..8)
           or binary array (=array([1,1,0,0,1,1,0,0]))
           or string (='all')
           or None (default), uses previous configuration
        
        Returns:
        None
        
        Drops tips in waste
        """
        if self.tipsloaded == False:
            print 'No tips loaded'
            return
        tips = self.__normalize__('mask', tips)
        
        # output
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self.mode == 'offline':
            wstr = "DropDITI("+str(int(sum(tipval)))+','+str(self.Objects['Waste'].grid)
            wstr = wstr+','+str(self.Objects['Waste'].site)+",10,70,0);"
            self.output.write(wstr)
            self.output.flush()
        self.tipsloaded = False
        return
    
    def transferLabware(self, source, destination, lid=None, uncover=True):
        """
        Parameters:
        source: Plate object
             or string, name of the labware
        destination: Plate object
                  or string, name of the labware
        lid: Plate object
          or string, name of the labware
          or None, if you do not want lid handling
        uncover: boolean, if true, uncover at destination
                          if false, cover at source
        
        Returns:
        None
        
        Move around plates
        """
        # Validate
        # select plates
        lidhandling = True
        source = self.__normalize__('plate',source)
        destination = self.__normalize__('plate',destination)
        if lid == None:
            lidhandling = False
            lid = Plate('tmp', self.Objects[source].types, "", "")
        lid = self.__normalize__('plate',lid)
        
        # output
        if self.mode == 'offline':
            wstr = "Transfer_Rack(\""+str(int(source.grid))+"\",\""+str(int(destination.grid))+"\",0,"+str(int(lidhandling))+",0,0,"+str(int(uncover))+",\""
            wstr = wstr +str(int(lid.grid))+"\",\""+self.Objects[source].types+"\",\""+"Narrow"+"\",\"\",\"\",\"MP 3Pos\",\"MP 3Pos\",\"MP 3Pos\",\""
            wstr = wstr +str(int(source.site))+"\",\""+str(int(lid.site))+"\",\""+str(int(destination.site))+"\");\n"
            self.output.write(wstr)
            self.output.flush()
        return
    
    def __del__(self):
        """
        Clean up, close file pointers or log off
        """
        self.output.flush()
        self.output.close()

    def __normalize__(self, what, param, **kwargs):
        """
        This overloaded function is used to convert different input
        forms to a standardized form for various cases
        """
        if what == 'tips':
            retval = param
            if isinstance(param, str):
                if param in self.tipcycle.keys():
                    retval = param
                else:
                    print 'Tip type not available'
                    retval = -1
            elif isinstance(param, TipBox):
                retval = param.types
            elif param == None: # TODO: add exception if there is no tip box
                retval = self.tipcycle.keys()[0]
            #print retval
            return retval
        if what == 'mask':
            retval = param
            if param == None:
                retval = self.tips
            elif param == 'all':
                retval = numpy.ones(8)
            elif isinstance(param,list):
                retval = numpy.zeros(8)
                for i in param:
                    retval[i-1] = 1
            #print retval
            return retval
        if what == 'volume':
            retval = param
            if (isinstance(param,float)) | (isinstance(param,int)):
                retval = []
                for i in range(8):
                    retval.append(param)
            if isinstance(param, list):
                retval = param.copy()
            #print retval
            return retval
        if what == 'plate':
            retval = param
            if not isinstance(param, Plate):
                retval = self.Objects[param]
            return retval

class Labware:
    """
    
    """
    name = None
    types = None
    grid = None
    site = None
    category = None
    xlen = 12
    ylen = 8
    
    def __init__(self, name, types, grid, site):
        self.name = name
        self.grid = grid
        self.site = site
        self.types = types

class TipBox(Labware):
    """
    
    """
    category = 'tipboxes'
    nTipsUsed = 0

class Trough(Labware):
    """
    
    """
    category = 'troughs'

class Plate(Labware):
    """
    
    """
    category = 'plates'
    occupied = 0

class Device:
    """
    
    """
    name = None
    types = None
    grid = None
    site = None
    parent = None
    category = 'devices'
    
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent

class Incubator(Device):
    """
    
    """
    occupied = numpy.zeros(8)
    
    def closeDoors(self):
        if self.parent.mode == 'offline':
            self.parent.output.write("FACTS(\"Incubator1\",\"Incubator1_CloseDoor\",\"\",\"0\",\"\");\n")
            self.parent.output.flush()
        return
    
    def setTemperature(self, temperature, slots):
        """
        Requires the slots argument to be a list, not an integer
        Say setTemperature(30, [2]), not setTemperature(30, 2)
        """
        if (slots == 'all') | (set(slots) == set([1,2,3,4,5,6])):
            slots = array([0])
        if self.parent.mode == 'offline':
            for slot in slots:
                wstr = "FACTS(\"Incubator1\",\"Incubator1_SetTemperature\",\""
                wstr = wstr+str(slot)+','+str(temperature)+"\",\"0\",\"\");\n"
                self.parent.output.write(wstr)
                self.parent.output.flush()
        #else:
            
        return

class Platereader(Device):
    """
    
    """
    occupied = 0
    
    def close(self):
        if self.parent.mode == 'offline':
            self.parent.output.write("FACTS(\"Sunrise\",\"Sunrise_Close\",\"\",\"0\",\"\");\n")
            self.parent.output.flush()
        return
    
    def open(self):
        if self.parent.mode == 'offline':
            self.parent.output.write("FACTS(\"Sunrise\",\"Sunrise_Open\",\"\",\"0\",\"\");\n")
            self.parent.output.flush()
        return
    
    def measure(self):
        return

def wellSelGen(start, tips, labware):
    """
    start: index where sequence tips starts
    tips: binary sequence
    """
    retstr = ''
    start = start - 1
    xlen = labware.xlen
    ylen = labware.ylen
    retstr = retstr + format(xlen,'02x').upper() + format(ylen,'02x').upper()
    nWells = xlen*ylen
    wells = numpy.zeros(nWells)
    wells[start:start+len(tips)] = tips
    igroup = 0
    while igroup < nWells:
        seq = numpy.zeros(7)
        seq[0:min(7,nWells-igroup)] = wells[igroup:igroup+min(7,nWells-igroup)]
        for i in range(len(seq)):
            seq[i] = seq[i]*2**i
        seqval = int(sum(seq))+48
        retstr = retstr + chr(seqval)
        igroup = igroup + 7
    return retstr

def __genTipOffsetPattern__(tips, wells):
    """
    Finds the pattern in wells within tips, and returns a new
    array of tips which is the wells array without the offset
    """
    # ensure well pattern is a subset of loaded tip pattern
    match = False
    minimalwell = wells[min(numpy.where(wells==1)[0]):max(numpy.where(wells==1)[0])]
    if len(minimalwell) > 8:
        return -1
    for offset in range(8-len(minimalwell)):
        match = True
        for i in range(len(minimalwell)):
            if (minimalwell[i] == 1) & (tips[i+offset] == 0):
                match = False
                break
        if match == True:
            break
    if match == False:
        return -1
    for i in range(len(minimalwell)):
        if minimalwell[i] == 0:
            tips[i+offset] == 0
    return tips

